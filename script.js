const redTextCheck = document.querySelector('.redText');
const buttonCheck = document.querySelector('.btn');
const allInput = document.querySelectorAll('input');
const allQuery = document.querySelectorAll('.icon-password');
allQuery.forEach((eye) => {
  eye.addEventListener('click', (e) => {
    const selectSome = e.currentTarget.previousSibling.previousSibling;
    if (selectSome.type === 'password') {
      selectSome.type = 'text';
      eye.classList.replace('fa-eye', 'fa-eye-slash');
    } else {
      selectSome.type = 'password';
      eye.classList.replace('fa-eye-slash', 'fa-eye');
    }
  });
});
buttonCheck.addEventListener('click', (e) => {
  e.preventDefault();
  if (allInput[0].value === allInput[1].value) {
    alert('You are welcome');
  } else {
    redTextCheck.innerText = 'Потрібно ввести однакові значення';
    redTextCheck.style.color = '#ff0000';
  }
});
